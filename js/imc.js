document.addEventListener("DOMContentLoaded", function () {
    const btnCalcular = document.getElementById("btnCalcular");
    const resultadoIMC = document.getElementById("IMC");
    const leyenda = document.getElementById("leyenda");
    const caloriasRecomendadas = document.getElementById("calorias-recomendadas");
    const imagen = document.getElementById("imagen");

    btnCalcular.addEventListener("click", function () {
        let altura = parseFloat(document.getElementById('altura').value);
        let peso = parseFloat(document.getElementById('Peso').value);
        let edad = parseInt(document.getElementById('edad').value);
        let sexo = document.querySelector('input[name="sexo"]:checked').value;

        if (!isNaN(altura) && !isNaN(peso) && !isNaN(edad)) {
            let imc = peso / (altura * altura);
            resultadoIMC.value = imc.toFixed(2);

            // Calculate calories based on age and gender
            let calories = 0;

            if (edad < 18) {
                if (sexo === 'hombre') {
                    calories = 17.686 * peso + 658.2;
                } else if (sexo === 'mujer') {
                    calories = 13.384 * peso + 692.6;
                }
            } else if (edad >= 18 && edad < 30) {
                if (sexo === 'hombre') {
                    calories = 15.057 * peso + 692.2;
                } else if (sexo === 'mujer') {
                    calories = 14.818 * peso + 486.6;
                }
            } else if (edad >= 30 && edad < 60) {
                if (sexo === 'hombre') {
                    calories = 11.472 * peso + 873.1;
                } else if (sexo === 'mujer') {
                    calories = 8.126 * peso + 845.6;
                }
            } else if (edad >= 60) {
                if (sexo === 'hombre') {
                    calories = 11.711 * peso + 587.7;
                } else if (sexo === 'mujer') {
                    calories = 9.082 * peso + 658.5;
                }
            }

            leyenda.textContent = `Calorías recomendadas: ${calories.toFixed(2)}`;
            caloriasRecomendadas.textContent = `Calorías recomendadas: ${calories.toFixed(2)}`;

            if (imc < 18.5) {
                leyenda.textContent = "Bajo peso";
                imagen.src = "/img/01.png"; // Ruta de la imagen de bajo peso
                imagen.alt = "Bajo peso";
            } else if (imc >= 18.5 && imc < 25) {
                leyenda.textContent = "Adecuado";
                imagen.src = "/img/02.png"; // Ruta de la imagen de adecuado
                imagen.alt = "Adecuado";
            } else if (imc >= 25 && imc < 30) {
                leyenda.textContent = "Sobrepeso";
                imagen.src = "/img/03.png"; // Ruta de la imagen de sobrepeso
                imagen.alt = "Sobrepeso";
            } else if (imc >= 30 && imc < 35) {
                leyenda.textContent = "Obesidad grado 1";
                imagen.src = "/img/04.png"; // Ruta de la imagen de obesidad grado 1
                imagen.alt = "Obesidad grado 1";
            } else if (imc >= 35 && imc < 40) {
                leyenda.textContent = "Obesidad grado 2";
                imagen.src = "/img/05.png"; // Ruta de la imagen de obesidad grado 2
                imagen.alt = "Obesidad grado 2";
            } else {
                leyenda.textContent = "Obesidad grado 3";
                imagen.src = "/img/06.png"; // Ruta de la imagen de obesidad grado 3
                imagen.alt = "Obesidad grado 3";
            }
        } else {
            alert("Por favor, ingrese valores numéricos válidos para altura, peso y edad.");
        }
    });
});
