/*declarar variables*/

const btnCalcular = document.getElementById("btnCalcular");

btnCalcular.addEventListener("click", function () {
    //obtener los datos de los input
    let valorAuto=document.getElementById('valorAuto').value;
    let pInicial=document.getElementById('Porcentaje').value;
    let plazos=document.getElementById('plazos').value;
    
    

    //hacer los calculos
    let pagoInicial=valorAuto*(pInicial/100);
    let totalFin=valorAuto-pagoInicial;
    let pagoMensual=totalFin/plazos;
    
    //mostrar los datos
    document.getElementById('pagoInicial').value=pagoInicial;
    document.getElementById('totalfin').value=totalFin;
    document.getElementById('pagoMensual').value=pagoMensual;

    
});

const btnLimpiar = document.getElementById("btnLimpiar");

btnLimpiar.addEventListener("click", function () {
    
    document.getElementById('valorAuto').value = "";
    document.getElementById('Porcentaje').value = "";
    document.getElementById('plazos').value = "";

  
    document.getElementById('pagoInicial').value = "";
    document.getElementById('totalfin').value = "";
    document.getElementById('pagoMensual').value = "";
});

const historial = document.getElementById("historial");

btnCalcular.addEventListener("click", function () {
    
    let valorAuto = parseFloat(document.getElementById('valorAuto').value);
    let pInicial = parseFloat(document.getElementById('Porcentaje').value);
    let plazos = parseInt(document.getElementById('plazos').value);

    
    let pagoInicial = valorAuto * (pInicial / 100);
    let totalFin = valorAuto - pagoInicial;
    let pagoMensual = totalFin / plazos;

    
    let listItem = document.createElement("li");
    listItem.textContent = `Pago Inicial: $${pagoInicial.toFixed(2)}, Total Financiado: $${totalFin.toFixed(2)}, Pago Mensual: $${pagoMensual.toFixed(2)}`;

   
    historial.appendChild(listItem);

    
    document.getElementById('valorAuto').value = "";
    document.getElementById('Porcentaje').value = "";
    document.getElementById('plazos').value = "";
});
