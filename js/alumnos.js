const alumnos =[{
    "matricula":"2020030714",
    "nombre":"Ruiz Guerrero Axel Jovani",
    "grupo":"7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":"/img/2020030714.jpg"
},{
    "matricula":"2021030136",
    "nombre":"Oscar Alejandro Solis Velarde",
    "grupo":"7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":"/img/2021030136.jpg"
},{
    "matricula": "2020030321",
    "nombre": "Ontiveros Govea Yair Alejandro",
    "grupo": "7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "/img/2020030321.jpg"
},{
    "matricula": "2021030262",
    "nombre": "Qui Mora Ángel Ernesto",
    "grupo": "7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "/img/2021030262.jpg"
},{
    "matricula": "2020030550",
    "nombre": "JESUS ADOLFO FLORES PEREZ",
    "grupo": "7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "/img/2020030550.jpg"
},{
    "matricula": "2021030314",
    "nombre": "Felipe Andrés Peñaloza Pizarro",
    "grupo": "7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "/img/2021030314.jpg"
}
,{
    "matricula": "2021030077",
    "nombre": "Mateo Arias Tirado",
    "grupo": "7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "/img/2021030077.jpg"
},{
    "matricula":"2021030008",
    "nombre":"Landeros Andrade María Estrella ",
    "grupo":"7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":"/img/2021030008.jpg"
},{
    "matricula": "2021030266",
    "nombre": "José Manuel González Ramírez",
    "grupo": "7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "/img/2021030266.jpg"
},{
    "matricula": "mosuna",
    "nombre": "Melissa Osuna Cárdenas",
    "grupo": "7-3 - Tutora",
    "carrera": "Tecnologías de la Información",
    "foto": "/img/mosuna.jpg"
}
];

document.addEventListener("DOMContentLoaded", function () {
    const alumnosList = document.getElementById("alumnos-list");

    alumnos.forEach(alumno => {
        const div = document.createElement("div");
        div.className = "alumno";
        div.innerHTML = `
            <img src="${alumno.foto}" alt="${alumno.nombre}" style="margin-left=20px">
            <h2>${alumno.nombre}</h2>
            <br>
            <p>Matrícula: <br> ${alumno.matricula}</p>
            
            <p>Carrera:<br> ${alumno.carrera}</p>
            <br>
            <p>Grupo:<br> ${alumno.grupo}</p>
            <br>
            `;
        alumnosList.appendChild(div);
    });
});


/*

let cuentaBanco = {
    "numero":"10001",
    "banco":"Banorte",
    cliente:{
        "nombre":"Axel ruiz",
        "fechaNac":"2002-27-02",
        "sexo":"M"
    },
    "saldo":"10000"
}

console.log("Nombre: "+cuentaBanco.cliente.nombre);
console.log("Saldo: "+cuentaBanco.saldo)

let productos=[{
"codigo":"1001",
"descripcion":"atun",
"precio":"34"
},{
    "codigo":"1002",
"descripcion":"jabon de polvo",
"precio":"23"
},{"codigo":"1003",
"descripcion":"Harina",
"precio":"43"},{"codigo":"1004",
"descripcion":"Pasta dental",
"precio":"78"}]

//Mostrar Atributos de un objeto del arreglo
for(let i=0;i<productos.length;i++){
    console.log("El codigo es: " +productos[i].codigo);
    console.log("El Descripcion es: " +productos[i].descripcion);
    console.log("El precio es: " +productos[i].precio);
}
*/