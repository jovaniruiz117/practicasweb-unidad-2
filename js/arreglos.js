const btnGenerar = document.querySelector('.btn-generar');
const inpCantidad = document.querySelector('.inp-number');
const combo = document.querySelector('.combo');
const promedioSpan = document.querySelector('.promedio');
const mayorSpan = document.querySelector('.mayor');
const menorSpan = document.querySelector('.menor');
const simetricoSpan = document.querySelector('.simetrico');
const paresSpan = document.querySelector('.pares');
const imparesSpan = document.querySelector('.impares');

// Initialize an empty array
let arreglo = [];

// Function to display the array elements and its size
const mostrarArreglo = arreglo => {
    console.log("Elementos del arreglo:", arreglo);
    console.log("Tamaño del arreglo:", arreglo.length);
}

// Function to display the average of array elements
const mostrarPromedio = arreglo => {
    if (arreglo.length === 0) {
        return 0;
    }
    const promedio = arreglo.reduce((sum, item) => sum + item, 0) / arreglo.length;
    return promedio;
}

// Function to display the even values from an array
const mostrarValoresPares = arreglo => {
    return arreglo.filter(item => item % 2 === 0);
}

// Function to display the maximum value and its index in an array
const mostrarMayor = arreglo => {
    if (arreglo.length === 0) {
        return "El arreglo está vacío.";
    }
    let max = -Infinity;
    let index = -1;
    arreglo.forEach((item, i) => {
        if (item > max) {
            max = item;
            index = i;
        }
    });
    // Añadir 1 al valor del índice para que comience en 1.
    index += 1;
    return `Mayor: ${max}, Índice: ${index}`;
}

// Function to fill the array with random integers
const generarAleatorios = cantidad => {
    arreglo = [];
    for (let i = 0; i < cantidad; i++) {
        arreglo.push(Math.floor(Math.random() * 10));
    }
    return arreglo;
}

// Function to display the minimum value and its index in an array
const mostrarMenor = arreglo => {
    if (arreglo.length === 0) {
        return "El arreglo está vacío.";
    }
    let menor = Infinity;
    let index = -1;
    arreglo.forEach((item, i) => {
        if (item < menor) {
            menor = item;
            index = i;
        }
    });
    index+=1;
    return `Menor: ${menor}, Índice: ${index}`;
}

// Function to check if the array generator is symmetric
const esSimetrico = arreglo => {
    const cantidad = arreglo.length;
    if (cantidad === 0) {
        return false;
    }

    const paresCount = arreglo.filter(item => item % 2 === 0).length;
    const imparesCount = cantidad - paresCount;

    const paresPercentage = (paresCount / cantidad * 100).toFixed(0);
    const imparesPercentage = (imparesCount / cantidad * 100).toFixed(0);

    paresSpan.innerText = `Porcentaje de pares: ${paresPercentage}%`;
    imparesSpan.innerText = `Porcentaje de impares: ${imparesPercentage}%`;

    return Math.abs(paresPercentage - imparesPercentage) <= 20;
}

btnGenerar.addEventListener('click', e => {
    combo.innerHTML = '';
    const cantidad = parseInt(inpCantidad.value);

    if (cantidad >= 2) {
        generarAleatorios(cantidad);
        arreglo.forEach(i => {
            const item = document.createElement('option');
            item.value = i;
            item.innerText = i;
            combo.appendChild(item);
        });

        const promedio = mostrarPromedio(arreglo);
        const valoresPares = mostrarValoresPares(arreglo);
        const mayor = mostrarMayor(arreglo);
        const menor = mostrarMenor(arreglo);
        const simetrico = esSimetrico(arreglo);

        promedioSpan.innerText = `Promedio: ${promedio}`;
        mayorSpan.innerText = mayor;
        menorSpan.innerText = menor;
        simetricoSpan.innerText = `Simétrico: ${simetrico ? 'Sí' : 'No'}`;
    } else {
        alert('La cantidad debe ser al menos 2.');
    }
});
